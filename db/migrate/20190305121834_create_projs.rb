class CreateProjs < ActiveRecord::Migration[5.2]
  def change
    create_table :projs do |t|
      t.string :name
      t.string :email
      t.string :contact
      t.string :provider

      t.timestamps
    end
  end
end
