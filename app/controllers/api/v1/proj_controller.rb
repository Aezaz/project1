
        class Api::V1::ProjController < ApplicationController
            
            def index
                proj = Proj.order('created_at');
                render json: {status: "success",message: "added",data: proj},status: :ok
            end

            def proj_input
                params.permit(:name , :email , :contact , :provider)
              end

              def create
                proj = Proj.new(proj_input)
                if proj.save
                  render json: {status: "Success",message: "saved",data: proj}, status: :ok
                else 
                  render json: {status: "Failed",message: "Not saved",data: proj.errors}, status: :unprocessable_entity 
                end
            end

            def update
                proj=Proj.find(params[:id])
                if proj.update_attributes(proj_input)
                  render json: {status: "success",message: "updated", data: proj}, status: :ok
                else
                  render json: {status: "failed",message: "unable to update", data: proj.errors}, status: :unprocessable_entity
                end
              end
        end
  

